package org.dmcmanam.console_chess;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ChessBoardTest {
	@Test
	void testGetColumnFromAlgebraic() {
		int column = ChessBoard.getColumnFromAlgebraic("d2");
		assertEquals(3, column);
	}

	@Test
	void testGetRowFromAlgebraic() {
		int row = ChessBoard.getRowFromAlgebraic("d2");
		assertEquals(6, row);
	}

	@Test
	void testGetRowFromAlgebraic5() {
		int row = ChessBoard.getRowFromAlgebraic("d5");
		assertEquals(3, row);
	}
	@Test
	void testGetRowFromAlgebraicRightTopCorner() {
		int row = ChessBoard.getRowFromAlgebraic("h8");
		assertEquals(0, row);
	}
	
	@Test
	void testGetRowFromAlgebraicRightBottomCorner() {
		int row = ChessBoard.getRowFromAlgebraic("h1");
		assertEquals(7, row);
	}
	
	@Test
	void testNotationIsCorrectForCapture() {
		assertTrue(ChessBoard.notationIsCorrect("Rd3xd7"));
	}
}