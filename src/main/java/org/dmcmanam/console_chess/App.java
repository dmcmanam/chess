package org.dmcmanam.console_chess;

import java.util.Scanner;

/**
 * Instantiates the game, reads new moves from the command line and calls move(), prints the board until the game is over.
 */
public class App {
	public static void main(String [] args) {
		ChessBoard game = new ChessBoard();
		System.out.println(game);
	
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println(ChessBoard.getRules());
			while (true) {
				System.out.println(game.whoseTurn() + " it is your move.");
				
				String nextMove = scanner.nextLine();
				if (nextMove.length() < 4)
					System.out.println(ChessBoard.getRules());
				else if (game.move(nextMove)) {
					System.out.println(game);
				}
			}
		}
	}
}