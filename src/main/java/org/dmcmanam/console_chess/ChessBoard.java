package org.dmcmanam.console_chess;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
  Console based chess game with ASCII art chess pieces:

     A       B       C       D       E       F       G       H
  ------- ------- ------- ------- ------- ------- ------- -------
 | @___@ |  %~b  |  .@.  | \o*o/ | __+__ |  .@.  |  %~b  | @___@ |
8|  @@@  | `'dX  |  @@@  |  @@@  | `@@@' |  @@@  | `'dX  |  @@@  |8
 | d@@@b |  d@@b | ./A\. | d@@@b | d@@@b | ./A\. |  d@@b | d@@@b |
  ------- ------- ------- ------- ------- ------- ------- -------
 |   _   |   _   |   _   |   _   |   _   |   _   |   _   |   _   |
7|  (@)  |  (@)  |  (@)  |  (@)  |  (@)  |  (@)  |  (@)  |  (@)  |7
 |  d@b  |  d@b  |  d@b  |  d@b  |  d@b  |  d@b  |  d@b  |  d@b  |
  ------- ------- ------- ------- ------- ------- ------- -------
 |       | . . . |       | . . . |       | . . . |       | . . . |
6|       | . . . |       | . . . |       | . . . |       | . . . |6
 |       | . . . |       | . . . |       | . . . |       | . . . |
  ------- ------- ------- ------- ------- ------- ------- -------
 | . . . |       | . . . |       | . . . |       | . . . |       |
5| . . . |       | . . . |       | . . . |       | . . . |       |5
 | . . . |       | . . . |       | . . . |       | . . . |       |
  ------- ------- ------- ------- ------- ------- ------- -------
 |       | . . . |       | . . . |       | . . . |       | . . . |
4|       | . . . |       | . . . |       | . . . |       | . . . |4
 |       | . . . |       | . . . |       | . . . |       | . . . |
  ------- ------- ------- ------- ------- ------- ------- -------
 | . . . |       | . . . |       | . . . |       | . . . |       |
3| . . . |       | . . . |       | . . . |       | . . . |       |3
 | . . . |       | . . . |       | . . . |       | . . . |       |
  ------- ------- ------- ------- ------- ------- ------- -------
 |   _   |   _   |   _   |   _   |   _   |   _   |   _   |   _   |
2|  ( )  |  ( )  |  ( )  |  ( )  |  ( )  |  ( )  |  ( )  |  ( )  |2
 |  /_\  |  /_\  |  /_\  |  /_\  |  /_\  |  /_\  |  /_\  |  /_\  |
  ------- ------- ------- ------- ------- ------- ------- -------
 | [___] |  %~\  |  .O.  | \o^o/ | __+__ |  .O.  |  %~\  | [___] |
1|  [ ]  | `')(  |  \ /  |  [ ]  | `. .' |  \ /  | `')(  |  [ ]  |1
 | /___\ |  <__> |  /_\  | /___\ | /___\ |  /_\  |  <__> | /___\ |
  ------- ------- ------- ------- ------- ------- ------- -------
     A       B       C       D       E       F       G       H
 
 ASCII board and chess piece design from: 
 https://stackoverflow.com/questions/19517374/pythonic-way-to-print-a-chess-board-in-console
 
 Moves are based on long algebraic notation in English.
 * 
 * @author David McManamon
 */
public class ChessBoard {
	/*
	 * The board is modeled as 64 pieces - black and white empty spaces occupy all spots without chess pieces.
	 */
	private ChessPiece [][] pieces = new ChessPiece[8][8];
	public enum Side {BLACK, WHITE};  // standard names for the 2 sides
	private Side turn = Side.WHITE;   // white goes first
	
	public ChessBoard() {
		// setup all 16 white & black pieces on the board and color empty places black or white
		for(int i=0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (i == 0)
					pieces[0] = getBlackFirstRow();
				else if (i == 1)
					pieces[1][j] = new BlackPawn();
				else if (i== 6)
					pieces[6][j] = new WhitePawn();
				else if (i == 7)
					pieces[7] = getWhiteFirstRow();
				else {
					pieces[i][j] = getEmptySpace(i,j);
				}
			}
		}
	}
	
	public Side whoseTurn() {
		return turn;
	}
	
	public static String getRules() {
		String rules = "Enter a move in long algebraic notation such as 'e2-e4' or 'Nb1-c3' or 'Pass' or 'Resign' to quit and press RETURN.\n" +
			"Castling is indicated by the special notations 0-0 (for kingside castling) and 0-0-0 (queenside castling).\n" +
			"Checkmate at the completion of moves is represented by the symbol \"# \" which ends the game.";
		return rules;
	}
	
	/**
	 * A move in long algebraic notation. 
	 * Moves specify both the starting and ending squares separated by a hyphen, 
	 * for example: e2-e4 or Nb1-c3
	 * 
	 * Castling is indicated by the special notations 0-0 (for kingside castling) and 0-0-0 (queenside castling).
	 * 
	 * English-speaking players use the letters K for king, Q for queen, R for rook, B for bishop, and N for knight (since K is already used)
	 * @param algebraicNotation
	 */
	public boolean move(String algebraicNotation) {
		// first screen special moves
		if ("Resign".equals(algebraicNotation)) {
			System.exit(0);
		} else if ("Pass".equals(algebraicNotation)) {
			changeTurn();
			return true;
		} else if ("0-0".equals(algebraicNotation))
			return kingsideCastle();
		else if ("0-0-0".equals(algebraicNotation))
			return quensideCastle();
		
		if (!notationIsCorrect(algebraicNotation)) {
			System.err.println("Long algebraic notation must be for the form [letter][number]-[letter][number] where letters are a-h & numbers are 1-8.");
			return false;
		}
		char piece = 'P';
		if (Character.isUpperCase(algebraicNotation.charAt(0))) {
			piece = algebraicNotation.charAt(0); // remove the letter K, Q, R, B or N
			algebraicNotation = algebraicNotation.substring(1);
		}
		String [] move = algebraicNotation.split("-");
		String from = move[0];
		String to = move[1];
		
		int fromColumn = getColumnFromAlgebraic(from);
		int fromRow = getRowFromAlgebraic(from);
		int toRow = getRowFromAlgebraic(to);
		int toColumn = getColumnFromAlgebraic(to);
		ChessPiece moveFrom = pieces[fromRow][fromColumn];
		if (piece != moveFrom.letter || moveFrom.side != whoseTurn()) {
			System.err.println("Move incorrect for turn and piece locations. Long algebraic notation must be for the form [letter][number]-[letter][number] where letters are a-h & numbers are 1-8.");
			return false;
		}
		pieces[toRow][toColumn] = moveFrom;
		pieces[fromRow][fromColumn] = getEmptySpace(fromRow,fromColumn);
		
		changeTurn();
		return true;
	}
	
	protected boolean quensideCastle() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean kingsideCastle() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * When a pawn moves to the last rank and promotes, 
	 * the piece promoted to is indicated at the end of the move notation, for example: e8Q (promoting to queen)
	 */
	protected void promotePawn() {
		// TODO
	}
	
	protected void enPassantCapture() {
		// TODO
	}
	
	protected void checkmate() {
		//TODO
	}

	protected void changeTurn() {
		if (turn == Side.WHITE)
			turn = Side.BLACK;
		else
			turn = Side.WHITE;
	}

	private ChessPiece[] getWhiteFirstRow() {
		ChessPiece[] whiteArmy = {
				new ChessPiece("Rook", Side.WHITE, 'R', " [___] ", "  [ ]  ", " /___\\ "),
				new ChessPiece("Knight", Side.WHITE, 'N', "  %~\\  ", " `')(  ", "  <__> "),
				new ChessPiece("Bishop", Side.WHITE, 'B', "  .O.  ", "  \\ /  ", "  /_\\  "),
				new ChessPiece("Queen", Side.WHITE, 'Q', " \\o^o/ ","  [ ]  ", " /___\\ "),
				new ChessPiece("King", Side.WHITE, 'K', " __+__ "," `. .' ", " /___\\ "),
				new ChessPiece("Bishop", Side.WHITE, 'B', "  .O.  ", "  \\ /  ", "  /_\\  "),
				new ChessPiece("Knight", Side.WHITE, 'N', "  %~\\  ", " `')(  ", "  <__> "),
				new ChessPiece("Rook", Side.WHITE, 'R', " [___] ", "  [ ]  ", " /___\\ ") };
		return whiteArmy;
	}
	
	private ChessPiece[] getBlackFirstRow() {
		ChessPiece[] blackArmy = {
				new ChessPiece("Rook", Side.BLACK, 'R'," @___@ ", "  @@@  ", " d@@@b "),
				new ChessPiece("Knight", Side.BLACK, 'N',"  %~b  ", " `'dX  ", "  d@@b "), 
				new ChessPiece("Bishop", Side.BLACK, 'B',"  .@.  ", "  @@@  ", " ./A\\. "),
				new ChessPiece("Queen", Side.BLACK, 'Q', " \\o*o/ ","  @@@  ", " d@@@b "), 
				new ChessPiece("King", Side.BLACK, 'K', " __+__ "," `@@@' ", " d@@@b "), 
				new ChessPiece("Bishop", Side.BLACK, 'B',"  .@.  ", "  @@@  ", " ./A\\. "),
				new ChessPiece("Knight", Side.BLACK, 'N',"  %~b  ", " `'dX  ", "  d@@b "),  
				new ChessPiece("Rook", Side.BLACK, 'R'," @___@ ", "  @@@  ", " d@@@b ") };
		return blackArmy;
	}

	/*
	 * Returns the correct black or white empty space for the row & column given.
	 */
	protected static ChessPiece getEmptySpace(int row, int column) {
		if (row % 2 == 0) {
			if (column % 2 == 0)
				return new EmptyWhiteSpace();
			else
				return new EmptyBlackSpace();
		} else {
			if (column % 2 == 0)
				return new EmptyBlackSpace();
			else
				return new EmptyWhiteSpace();
		}
	}
	
	// Captures are indicated using "x": Rd3xd7
	protected static boolean notationIsCorrect(String algebraicNotation) {
		Pattern p = Pattern.compile("[KQRBN]?[a-h][1-8][-|x][a-h][1-8]");
		Matcher m = p.matcher(algebraicNotation);
		return m.matches();
	}

	protected static int getColumnFromAlgebraic(String x) {
		char letter = x.charAt(x.length()-2); 
		// ascii a = 97
		int columnIndex = letter - 97;
		return columnIndex;
	}

	protected static int getRowFromAlgebraic(String x) {
		int [] algebraicToJavaArrayIndex = {0, 7, 6, 5, 4, 3, 2, 1, 0};
		int rowAlgebraic = Character.digit(x.charAt(x.length()-1), 10);
		return algebraicToJavaArrayIndex[rowAlgebraic];
	}
	
	private static class ChessPiece {
		public String name;
		public Side side;
		public char letter;
		public String asciiRow1;
		public String asciiRow2;
		public String asciiRow3;
		
		protected ChessPiece(String name, Side side, char letter, String asciiRow1, String asciiRow2, String asciiRow3) {
			this.name = name;
			this.side = side;
			this.letter = letter;
			this.asciiRow1 = asciiRow1;
			this.asciiRow2 = asciiRow2;
			this.asciiRow3 = asciiRow3;
		}
	}
	
	static class EmptyBlackSpace extends ChessPiece {
		public EmptyBlackSpace() {
			super("empty", null, ' ', " . . . "," . . . ", " . . . ");
		}
	}
	
	static class EmptyWhiteSpace extends ChessPiece {
		public EmptyWhiteSpace() {
			super("empty", null, ' ', "       ","       ", "       ");
		}
	}
	
	static class BlackPawn extends ChessPiece {
		public BlackPawn() {
			super("Pawn", Side.BLACK, 'P', "   _   ","  (@)  ", "  d@b  ");
		}
	}
	
	static class WhitePawn extends ChessPiece {
		public WhitePawn() {
			super("Pawn", Side.WHITE, 'P', "   _   ","  ( )  ", "  /_\\  ");
		}
	}

	@Override
	public String toString() {
		// switch to StringBuilder later
		String LETTERS = "    A       B       C       D       E       F       G       H \n";
		String DASHES = "  ------- ------- ------- ------- ------- ------- ------- ------- \n";
		String b = LETTERS + DASHES;
		int rowNum = 8;
		for (int row = 0; row < pieces.length; row++) {
			for (int i = 0; i < 3; i++) {
				if (i == 1)
					b += Integer.toString(rowNum) + "|";
				else
					b += " |";
				for (int column = 0; column < pieces.length; column++) {
					if (i == 0)
						b += pieces[row][column].asciiRow1 + "|";
					else if (i == 1)
						b += pieces[row][column].asciiRow2 + "|";
					else if (i == 2)
						b += pieces[row][column].asciiRow3 + "|";
				}
				if (i == 1) {
					b += Integer.toString(rowNum);
					rowNum--;
				}
				b += " \n";
			}
			b += DASHES;
		}
		b += LETTERS;
		return b;
	}
}