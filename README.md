# console-chess

This is the source for a console chess game.

This game is playable using long algebraic notation in English minus castling, en passent capture, pawn promotion and checkmate.

## Moving Towards a Production Quality Release
The published interface for ChessBoard is OO; however, the internal implementation of ChessBoard is not yet fully OO although readability should be high.  
It is my experience that the best & most maintainable code is built through refactoring so 
I would add the remaining missing moves then refactor areas where complexity is too high.